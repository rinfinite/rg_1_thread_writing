import java.io.*;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

    static BufferedWriter os = null;
    static final int inputSize = 10000;

    public static void main(String[] args) {
        generateInput();
        threadOutput();
    }

    public static void threadOutput() {
        BufferedReader is = null;
        String line;
        Stack stack = new Stack();
        Random random = new Random();
        try {
            is = new BufferedReader(new InputStreamReader(new FileInputStream("generateInput.txt")));
            os = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("output.txt")));
            while ((line = is.readLine()) != null) {
                stack.push(line);
            }

            // sozdayom pool potokov
            ExecutorService executorService = Executors.newFixedThreadPool(6);
            // poka v stacke yest elementi, peredayom na obrabotku k potoku iz pool-a
            while (!stack.empty()) {
                int count = random.nextInt(6) + 1;
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            for (int j = 0; j < count; j++) {

                                // count mojet imet bolshe znachenie chem kol-vo ostavshih elementov v stack-e,
                                // poetomu v takih sluchayah k kontsu vihodim
                                if (stack.empty()) break;
                                String line = Thread.currentThread().getName()
                                        + "  " + System.currentTimeMillis()
                                        + " " + count
                                        + "  " + stack.pop();
                                os.write(line + "\n");
                                os.flush();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            // proverka pered zakritiyem "os" stream-a, chto vse threadi zakonchili rabotu
            executorService.shutdown();
            try {
                while (!executorService.awaitTermination(2L, TimeUnit.SECONDS)) {
                    System.out.println("Waiting threads...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void generateInput() {
        BufferedWriter os = null;
        try {
            os = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("generateInput.txt")));
            for (int i = 1; i < inputSize; i++) {
                os.write("Item: " + i + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
